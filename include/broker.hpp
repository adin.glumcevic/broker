#pragma once
#include<vector>
#include"orders.hpp"

class Broker{
  private:
    Orders orders_;
    OrderBookView orderBook_;
    double sellPrice_ = 0;
    double buyPrice_ = 0;

    void extractMessInfo(const std::string&, std::vector<std::string>&);
    int analyseMess(const std::vector<std::string>&);
    int processAddMess(const std::vector<std::string>&, int);
    int processReduceMess(const std::vector<std::string>&, int);

  public:
    void listenFeed(int QQ);
};
