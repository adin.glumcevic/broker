#pragma once
#include<iostream>
#include<unordered_map>
#include<map>

class Order{
  public:
    int time_;
    std::string ID_;
    std::string side_;
    double price_;
    int quantity_;
  
};

class Orders{
  private:
    std::unordered_map<std::string, Order> orders_;
  
  public:
    int addOrder(Order);
    int reduceOrder(const std::string&, int, double&, int&, std::string&); 
};

class OrderBookView{
  private:
    std::map<double, int, std::greater<double>> buySide_;
    std::map<double, int> sellSide_;

  public:
    void addOnBuy(double, int);
    void addOnSell(double, int);
    void reduceOnBuy(double, int);
    void reduceOnSell(double, int);
    void checkBuySide(int, int, double&);
    void checkSellSide(int, int, double&);
};
