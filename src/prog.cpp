#include"broker.hpp"
#include"orders.hpp"

int main(int argc, char *argv[])
{
  if(argc != 2){
    std::cerr << "Input QQ, and only QQ!\n";
    return 1;
  }
 
  int QQ;
  try{
    QQ = std::stoi(argv[1]);
  }catch(...){
    std::cerr << "Bad QQ input!\n";
    return 1;
  }
  
  Broker glumac;
  glumac.listenFeed(QQ);

  return 0;
}
