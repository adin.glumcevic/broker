#include"broker.hpp"
#include<sstream>

// Vadi rijeci iz unesene poruke
void Broker::extractMessInfo(const std::string& message, std::vector<std::string>& messInfo){
  std::stringstream messageStream(message);
  std::string tmpInfo;
  
  while(messageStream >> tmpInfo){
    messInfo.push_back(tmpInfo);
  }
}

// Provjerava validnost unesene poruke. Vrati 1 ako je poruka tipa Add, 2 ako je poruka tipa Reduce, 0 ako je nevalidan unos
int Broker::analyseMess(const std::vector<std::string>& messInfo){
  int size = messInfo.size();
  if(size != 6 && size != 4){
    std::cerr << "Bad input!\n";
    return 0;
  }
  if(size == 6 && messInfo[1] == "A" && (messInfo[3] == "B" || messInfo[3] == "S"))
    return 1;
  if(size == 4 && messInfo[1] == "R")
    return 2;
  
  std::cerr << "Bad input!\n";
  return 0;
}

int Broker::processAddMess(const std::vector<std::string>& messInfo, int QQ){
  int time, quantity;
  double price;
  std::string ID, side;
  
  try{
    time = stoi(messInfo[0]);
    ID = messInfo[2];
    side = messInfo[3];
    price = stod(messInfo[4]);
    quantity = stoi(messInfo[5]);
  }
  catch(...){
    std::cerr << "Bad input!\n";
    return 0;
  }
  
  if(!orders_.addOrder(Order{time, ID, side, price, quantity})){
    std::cerr << "Bad input!\n";
    return 0;
  }
  if(side == "B"){
    orderBook_.addOnBuy(price, quantity);
    orderBook_.checkBuySide(time, QQ, sellPrice_);
  }
  else{ 
    orderBook_.addOnSell(price, quantity);
    orderBook_.checkSellSide(time, QQ, buyPrice_);
  }

  
  
  //std::cout << "ADD MESSAGE:\nTime - " << time << "\nID - " << ID << "\nSide - " << side << "\nPrice - " << price << "\nQuantity - " << quantity << std::endl << std::endl;

  return 1;
}

int Broker::processReduceMess(const std::vector<std::string>& messInfo, int QQ){
  int time, quantity, qntToReduce;
  double price;
  std::string ID, side;

  try{
    time = stoi(messInfo[0]);
    ID = messInfo[2];
    quantity = stoi(messInfo[3]);
  }
  catch(...){
    std::cerr << "Bad input!\n";
    return 0;
  }

  if(!orders_.reduceOrder(ID, quantity, price, qntToReduce, side)){
    std::cerr << "Bad input!\n";
    return 0;
  }
  if(side == "B"){
    orderBook_.reduceOnBuy(price, qntToReduce);
    orderBook_.checkBuySide(time, QQ, sellPrice_);
  }
  else{
    orderBook_.reduceOnSell(price, qntToReduce);
    orderBook_.checkSellSide(time, QQ, buyPrice_);
  }

  //std::cout << "REDUCE MESSAGE:\nTime - " << time << "\nID - " << ID << "\nQuantity - " << quantity << std::endl << std::endl;

  return 1;
}

void Broker::listenFeed(int QQ){
  std::string message;
  
  while(std::getline(std::cin, message)){ 
    std::vector<std::string> messInfo;
    
    extractMessInfo(message, messInfo);
    
    int messType = analyseMess(messInfo);
    if(messType == 0)
      continue;
    else if(messType == 1){
      if(!processAddMess(messInfo, QQ))
        continue;
    }else{
      if(!processReduceMess(messInfo, QQ))
        continue;
    }
  } 
}


