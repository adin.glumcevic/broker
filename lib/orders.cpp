#include"orders.hpp"
#include<iomanip>

int Orders::addOrder(Order order){
  auto it = orders_.find(order.ID_);
  if(it != orders_.end())
    return 0;
  
  orders_.insert({order.ID_, order});
  
  return 1;
}

int Orders::reduceOrder(const std::string& ID, int quantity, double& price, int& qntToReduce, std::string& side){
  auto it = orders_.find(ID);
  if(it == orders_.end())
    return 0;
  
  price = (it->second).price_;
  side = (it->second).side_;

  if((it->second).quantity_ <= quantity){
    qntToReduce = (it->second).quantity_;
    orders_.erase(it);
  }else{
    qntToReduce = quantity;
    (it->second).quantity_ -= quantity;
  }
  
  return 1;
}

void OrderBookView::addOnBuy(double price, int quantity){
  auto it = buySide_.find(price);
  if(it != buySide_.end())
    (it->second) += quantity;
  else
    buySide_[price] = quantity;
}

void OrderBookView::addOnSell(double price, int quantity){
  auto it = sellSide_.find(price);
  if(it != sellSide_.end())
    (it->second) += quantity;
  else
    sellSide_[price] = quantity;

}

void OrderBookView::reduceOnBuy(double price, int quantity){
  auto it = buySide_.find(price);
  if(it->second == quantity)
    buySide_.erase(it);
  else 
    it->second -= quantity;
}

void OrderBookView::reduceOnSell(double price, int quantity){
  auto it = sellSide_.find(price);
  if(it->second == quantity)
    sellSide_.erase(it);
  else 
    it->second -= quantity;
}

void OrderBookView::checkBuySide(int timestamp, int QQ, double& sellPrice){
  double price = 0;
  auto it = buySide_.begin();
  while(it != buySide_.end()){

    //std::cout << "TU SMO : " << it->first <<" "<< it->second <<std::endl;

    if(it->second < QQ){
      price += it->first * it->second;
      QQ -= it->second;
    }else{
      price += it->first * QQ;
      QQ = 0;
    }

    if(!QQ)
      break;
    ++it;
  }

  if(!QQ){
    if(sellPrice == price)
      return; 
    
    sellPrice = price;
    std::cout << timestamp << " S " << std::fixed << std::setprecision(2) << price << std::endl;
  }else{
    if(sellPrice == 0)
      return;
   
    sellPrice = 0;
    std::cout << timestamp << " S NA" << std::endl;
  }
}

void OrderBookView::checkSellSide(int timestamp, int QQ, double& buyPrice){
  double price = 0;
  auto it = sellSide_.begin();
  while(it != sellSide_.end()){
    
   // std::cout << "TU SMO : " << it->first <<" "<< it->second <<std::endl;

    if(it->second < QQ){
      price += it->first * it->second;
      QQ -= it->second;
    }else{
      price += it->first * QQ;
      QQ = 0;
    }

    if(!QQ)
      break;
    ++it;
  }

  if(!QQ){
    if(buyPrice == price)
      return; 
    
    buyPrice = price;
    std::cout << timestamp << " B " << std::fixed << std::setprecision(2) << price << std::endl;
  }else{
    if(buyPrice == 0)
      return;
   
    buyPrice = 0;
    std::cout << timestamp << " B NA" << std::endl;
  }

}

